<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package tinytrees
 */

?>

	<footer id="colophon" class="site-footer">
		<div class="site-info">
			<div id="footer-left" class="sidebar">
    			<?php dynamic_sidebar( 'sidebar-1' ); ?>
			</div>
			<div id="footer-right" class="sidebar">
    			<?php dynamic_sidebar( 'sidebar-2' ); ?>
			</div>
			
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
